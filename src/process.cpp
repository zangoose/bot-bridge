/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2020  Jack Phoebus <email>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "process.h"
#include <iostream>
#include <sstream>

#if _WIN32
#include <io.h>
using namespace std;

static void throwNotImplementedOnWindows(){
	throw runtime_error("Processes not yet implemented on Windows");
}

struct processPrivate{
};

process::process(){
	throwNotImplementedOnWindows();
}
process::~process(){
	throwNotImplementedOnWindows();
}

process::void addCallback(function<void(int,string,string)> f){
	throwNotImplementedOnWindows();
}
process::tuple<int,string,string> exec(string command,vector<string>& args){
	throwNotImplementedOnWindows();
}

#else
#include <mutex>
#include <unistd.h>
#include <spawn.h>
#include <sys/wait.h>
using namespace std;

constexpr auto buffsize=1024;

//TODO: could be worth introducing a class to handle pipes in a raii-friendly manner
struct processPrivate{
	int cout_pipe[2];
	int cerr_pipe[2];
	
	posix_spawn_file_actions_t action;
	
	bool pipesClosed[2]{false,false};
	void closePipe(int i){
		if(!pipesClosed[i]){
			close(cout_pipe[i]), close(cerr_pipe[i]);
		}
	}
	
	processPrivate(){
		//globalLock.lock();
		
		if(pipe(cout_pipe) || pipe(cerr_pipe)){
		throw runtime_error("pipe returned an error.");
		}
		
		posix_spawn_file_actions_init(&action);
		
		posix_spawn_file_actions_addclose(&action, cout_pipe[0]);
		posix_spawn_file_actions_addclose(&action, cerr_pipe[0]);
		
		posix_spawn_file_actions_adddup2(&action, cout_pipe[1], STDOUT_FILENO);
		posix_spawn_file_actions_adddup2(&action, cerr_pipe[1], STDERR_FILENO);
		posix_spawn_file_actions_addclose(&action, cout_pipe[1]);
		posix_spawn_file_actions_addclose(&action, cerr_pipe[1]);
		
		//cout<<cout_pipe[0]<<" "<<cout_pipe[1]<<" "<<cerr_pipe[0]<<" "<<cerr_pipe[1]<<endl;
	}
	~processPrivate(){
		closePipe(0);
		closePipe(1);
		
		posix_spawn_file_actions_destroy(&action);
	}
	
	tuple<int,string,string> waitForCompletion(pid_t pid){
		//cout<<"reading"<<endl;
	
		array<char,buffsize> buffer;
		stringstream result,errors;
		int bytes;
		while(bytes=read(cout_pipe[0],&buffer[0],buffsize)){
			//cout<<"read "<<bytes<<" bytes"<<endl;
			string part(buffer.data(),buffer.data()+bytes);
			result<<part;
		}
		
		while(bytes=read(cerr_pipe[0],&buffer[0],buffsize)){
			//cout<<"read "<<bytes<<" bytes"<<endl;
			string part(buffer.data(),buffer.data()+bytes);
			errors<<part;
		}
		
		//cout<<"done reading, waiting for process to finish"<<endl;
		
		int rc;
		waitpid(pid,&rc,0);
		
		return {rc,result.str(),errors.str()};
	}
};

process::process(){
	priv=new processPrivate();
}

process::~process(){
	delete priv;
}

//todo: consider using a mutex here
tuple<int,string,string> process::exec(string command,vector<string> args){
	vector<char*> argv{command.data()};
	for(auto& s:args){
		argv.push_back(s.data());
	}
	argv.push_back(nullptr);
	
	pid_t pid;
	//cout<<argv[0]<<" "<<&priv->action<<" "<<argv.data()<<" "<<environ<<endl;
	const int err=posix_spawnp(&pid,argv[0],&priv->action,nullptr,argv.data(),environ);
	if(err){
		//EFAULT
		throw runtime_error("posix_spawnp() failed: "+to_string(err)+" from command: "+argv[0]);
	}
	
	priv->closePipe(1);
	
	return priv->waitForCompletion(pid);
}
#endif
