#include "botbridge.h"
#include "process.h"
#include <iostream>
#include <filesystem>
#include <functional>
#include <fstream>
#include <algorithm>
using namespace std;

void BotBridge::addAllResponseHandlersTo(Bots::Bot* bot){
	//TODO: this can be a plain array in c++20
	const static vector<function<bool(BotBridge*,Bots::Message)>> handlers{
		&BotBridge::echo,
		&BotBridge::brige,
		&BotBridge::log,
		&BotBridge::passToProcesses
	};
	
	for(const auto& handler:handlers){
		bot->addResponseHandler(bind(handler,this,placeholders::_1));
	}
}

bool BotBridge::echo(const Bots::Message m){
	cout<<"("<<m.roomId<<") ["<<m.getDateAsString()<<"] "<<m.username<<": "<<m.body<<endl;
	return true;
}

bool BotBridge::brige(const Bots::Message m){
	const auto id=m.origin->label()+":"+m.roomId;
	
	const auto recipents=bridges.find(id);
	if(recipents==bridges.end()){
		return true;
	}
	
	//cout<<"Forwarding from "<<id<<" to "<<recipents->second.size()<<" rooms"<<endl;
	for(const auto& target:recipents->second){
		//cout<<"Sending to: "<<target<<endl;
		
		auto [label,room]=Bots::splitLabelAndRoom(target);
		
		//cout<<label<<endl;
		const auto bot=Bots::getBotFromLabel(label);
		//cout<<bot<<endl;
		
		if(bot){
			Bots::Message forward;
			forward.roomId=room;
			forward.body=m.username+": "+m.body;
			
			bot->sendMessage(forward);
		}else{
			cout<<"couldnt find: "<<target<<endl;
		}
	}
	
	return true;
}

static string filenameSanitize(string fname){
	static const string invalidChars="\\/:?\"<>|";
	static const char replacement='_';
	constexpr int maxlen=64;
	
	transform(fname.begin(),fname.end(),fname.begin(),[](char c){
		return invalidChars.find(c)!=string::npos?replacement:c;
	});
	
	return fname.substr(0,maxlen);
}

static filesystem::path getLogDir(const filesystem::path& root, const Bots::Message& m){
	if(root.empty()){
		return filesystem::path();
	}
	
	auto logfile=root/"logs";
	
	const auto botname=m.origin->label();
	if(botname!=""){
		logfile/=botname;
	}
	
	string roomIdentifier=filenameSanitize(m.roomId);
	if(m.roomName.length()){
		cout<<"Room Name: "<<filenameSanitize(m.roomName)<<endl;
		roomIdentifier+="_"+filenameSanitize(m.roomName);
	}
	
	logfile/=roomIdentifier;
	
	//TODO: c++20 introduces a lot of constructs to improve this
	//notably: the stream operator <<, year_month_day(), time_of_day(), and std::formatter<std::chrono::sys_time>
	//which would also allow for this function to return a string instead and do all of the heavy lifting
	const auto date=m.getDateObjectUtc();
	
	logfile/=to_string(date.tm_year+1900);
	logfile/=to_string(date.tm_mon+1);
	logfile/=to_string(date.tm_mday);
	
	return logfile;
}

void BotBridge::logToFileInCurrentDatabasePath(const Bots::Message m, std::string filename, std::string message){
	const auto root=Bots::getDatabasePath();
	
	if(!enableLogging || root.empty()){
		return;
	}
	
	const auto logDir=getLogDir(root,m);
	filesystem::create_directories(logDir);
	
	const auto logfile=logDir/filename;
	
	cout<<"logging message to: "<<logfile<<endl;
	
	ofstream log;
	log.open(logfile,ios_base::app);
	
	log<<message<<endl;
}

static string getMessageLogString(const Bots::Message m){
	return m.messageId+" "+m.getDateAsString()+" "+m.username+": "+m.body;
}

bool BotBridge::log(const Bots::Message m){
	logToFileInCurrentDatabasePath(m,"chat.txt",getMessageLogString(m));
	
	return true;
}

template<typename T>
static bool vectorContains(const vector<T>& vec,const T& val){
	return find(vec.begin(),vec.end(),val)!=vec.end();
}
//TODO: c++20 adds this as a member of string
static bool startsWith(const string& s,const string& prefix){
	if(prefix.length()>s.length()){
		return false;
	}
	if(prefix.length()==0){
		return true;
	}
	
	for(int i=0;i<prefix.length();i++){
		if(prefix[i]!=s[i]){
			return false;
		}
	}
	
	return true;
}

static unordered_map<string,function<string(const Bots::Message&)>> substitutions{
	{"%m",[](const Bots::Message& m){return m.body;}},
	{"%u",[](const Bots::Message& m){return m.username;}},
	{"%r",[](const Bots::Message& m){return m.roomId;}},
	{"%d",[](const Bots::Message& m){return Bots::getDatabasePath();}},
	{"%D",[](const Bots::Message& m){return getLogDir(Bots::getDatabasePath(),m);}}
};
static string makeSubstitutions(string s,const Bots::Message& m){
	stringstream result;
	
	while(s.length()){
		bool hasPattern=false;
		for(const auto& [pattern,replacement]:substitutions){
			if(startsWith(s,pattern)){
				hasPattern=true;
				result<<replacement(m);
				s.erase(0,pattern.length());
				break;
			}
		}
		
		if(!hasPattern){
			result<<s[0];
			s.erase(0,1);
		}
	}
	
	return result.str();
}

void BotBridge::passToProcess(const Bots::Message m, const externalProcessArgs proc){
	const auto identifier=m.origin->label()+":"+m.roomId;
	if(vectorContains(proc.blacklist,identifier) || (proc.whitelist.size() && !vectorContains(proc.whitelist,identifier))){
		return;
	}
	
	vector<string> args;
	for(const auto& arg:proc.args){
		args.push_back(makeSubstitutions(arg,m));
	}
	cout<<"running: "<<proc.executable<<endl;
	
	process p;
	auto [rc,stdout,stderr]=p.exec(proc.executable,args);
	
	cout<<"process exited with status "<<rc<<" and said:"<<endl<<stdout<<endl;
	
	if(rc==0 && proc.outputIsReply && stdout.length()){
		Bots::Message reply;
		reply.body=stdout;
		
		m.sendReply(reply);
	}
	
	if(stderr.length()){
		cout<<"process reported errors:"<<endl;
		cout<<stderr<<endl;
		
		stringstream loggedMessage;
		loggedMessage<<getMessageLogString(m)<<endl<<stderr;
		logToFileInCurrentDatabasePath(m,"errors.txt",loggedMessage.str());
	}
	
	//TODO handle proc.outputIsReply and proc.passMessageToStdin
}

bool BotBridge::passToProcesses(const Bots::Message m){
	//TODO: run the process in a separate thread with a callback so it doesnt slow down everything else
	for(auto& proc:externalProcesses){
		passToProcess(m,proc);
	}
	
	return true;
}
