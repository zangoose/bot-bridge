#include "gameobjects.h"
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
using namespace std;

static inline string join(const vector<string>& strings, const string& token){
	const int size=strings.size();

	if(size==0){
		return "";
	}

	string result=strings[0];
	
	for(int i=1;i<size;i++){
		result+=token+strings[i];
	}

	return result;
}

template<typename T>
static inline void initValuesForMap(const nlohmann::json& data,std::string key,unordered_map<string,T>& target){
	if(data.contains(key)){
		for(const auto& itr:data[key].items()){
			const string name=toLower(itr.key());
			
			T item(itr.value(),name);
			item.displayName=name;
			target.emplace(name,item);
		}
	}
}

GameRoom::GameRoom(const nlohmann::json& data,string room):hpDice(data["initialHp"]){
	const auto [label,rId]=Bots::splitLabelAndRoom(room);
	roomId=rId;
	cout<<"Adding game to "<<room<<endl;
	
	//TODO: make sure all status names are lowercased, and any checks against those names should be lowercased
	
	//load given data
	initValuesForMap(data,"possibleStatus",availableStatuses);
	initValuesForMap(data,"actions",availableActions);
	
	if(data.contains("allowStacking")){
		allowStacking=data["allowStacking"];
	}else{
		allowStacking=false;
	}
	
	if(data.contains("privledged")){
		for(const string user:data["privledged"]){
			privledgedUsers.insert(user);
		}
	}
	
	//load existing config if it exists, otherwise create it
	const auto root=Bots::getDatabasePath()/"games"/label;
	filesystem::create_directories(root);
	stateFile=root/(roomId+".json");
	
	try{
		if(filesystem::exists(stateFile)){
			ifstream f(stateFile);
			f>>roomState;
		}else{
			writeState();
		}
	}catch(const nlohmann::detail::parse_error& e){
		cout<<"State file: "<<stateFile<<" was corrupt, making a new one"<<endl;
		
		const auto backupName=stateFile.string()+".corrupt";
		int counter=0;
		
		if(filesystem::exists(backupName)){
			do{
				++counter;
			}while(filesystem::exists(backupName+"."+to_string(counter)));
			
			filesystem::rename(stateFile,backupName+"."+to_string(counter));
		}else{
			filesystem::rename(stateFile,backupName);
		}
		
		writeState();
	}
	
	//set up events
	bot=Bots::getBotFromLabel(label);
	if(!bot){
		throw std::runtime_error("Couldn't find bot "+label+" for game");
	}
}

void GameRoom::wireEvents(){
	if(eventsWired){
		return;
	}
	
	bot->addResponseHandler(bind(&GameRoom::CheckMessage,this,placeholders::_1));
	bot->addEventHandler(bind(&GameRoom::onUserJoin,this,placeholders::_1));
	bot->addLoopedTask(bind(&GameRoom::RunTimedEvents,this));
	
	eventsWired=true;
}

bool GameRoom::isPrivledgedUser(std::string username){
	return privledgedUsers.find(username)!=privledgedUsers.end();
}

bool GameRoom::isPlaying(std::string username){
	return roomState.contains(username);
}


void GameRoom::writeState(){
	//TODO: write to a temp file and move it over top of the old file
	ofstream f(stateFile);
	f<<roomState;
}

bool GameRoom::onUserJoin(ChatEvent e){
	//cout<<"Got join event: "<<e.roomId<<" "<<e.username<<endl;
	
	if(e.roomId!=roomId){
		return true;
	}
	
	//cout<<"Correct Room"<<endl;
	
	if(e.type==ChatEventType::Join && hardcore && !isPlaying(e.username)){
		initializePlayer(e.username);
	}
	
	if(e.type==ChatEventType::Leave){
		unique_lock l(roomStateLock);
		removePlayer(e.username);
		writeState();
	}
	
	return true;
}

void GameRoom::updateUserHp(std::string username, int value){
	if(value>0){
		roomState[username]["hp"]=value;
	}else{
		removePlayer(username);
	}
}
void GameRoom::removePlayer(std::string username){
	roomState.erase(username);
	
	if(hardcore){
		//TODO:remove the player
	}
}
void GameRoom::sendMessage(std::string message){
	Bots::Message m;
	m.roomId=roomId;
	m.body=message;
	bot->sendMessage(m);
}
string GameRoom::getStatusChangeText(std::string username,const std::vector<StatusResult>& changes){
	bool actionFailed=false;
	string failSource;
	string text;
	
	for(const auto& change:changes){
		if(change.damage>0){
			text+=username+" received "+to_string(change.damage)+" damage from "+change.source+"\n";
		}else if(change.damage<0){
			text+=username+" healed "+to_string(-change.damage)+" health from "+change.source+"\n";
		}
		
		if(change.negateAction){
			actionFailed=true;
			failSource=change.source;
		}
	}
	
	if(text.length()){
		text+="\n";
	}
	
	if(actionFailed){
		//TODO: remove original message and report the attempt?
		if(text.length()){
			text+="Additionally, the last action of ";
		}else{
			text+="The last action of ";
		}
		
		text+=username+" has failed due to "+failSource+"\n\n";
	}
	
	return text;
}

template<typename T,typename U,class V>
static inline void eraseIf(unordered_map<T,U>& u_map,V predicate){
	for(auto it = begin(u_map); it != end(u_map);){
		if (predicate(it)){
			it = u_map.erase(it); // previously this was something like m_map.erase(it++);
		}else{
			++it;
		}
	}
}

bool GameRoom::RunTimedEvents(){
	unique_lock l(roomStateLock);
	//delete any timings for statuses that no longer exist
	//TODO: c++20 introduces erase_if which simplifies this
	
	//if the user is no longer playing, remove the entries
	eraseIf(statusLastAppliedTimestamp,[&](const auto& itr){
		return !isPlaying(itr->first);
	});
	
	//if any given user's status has expired, clear it out
	for(auto& [username,statuses]:statusLastAppliedTimestamp){
		auto& statusesJson=roomState[username]["statuses"];
		
		eraseIf(statuses,[&](const auto& itr){
			return !statusesJson.contains(itr->first);
		});
	}
	
	unordered_map<string,vector<StatusResult>> effectReport;
	unordered_map<string,vector<string>> statusesCured;
	
	//for each user, check if it's time for any given status to take effect
	for(const auto& [username,userData]:roomState.items()){
		vector<StatusResult> effects;
		vector<string> toRemove;
		auto& user=statusLastAppliedTimestamp[username];
		
		for(const auto& [statusName,statusData]:userData["statuses"].items()){
			Status status(statusData,statusName);
			
			if(status.rate.count()==0){
				continue;
			}
			
			auto statusEntry=user.find(status.displayName);
			
			const auto now=chrono::steady_clock::now();
			
			if(statusEntry==user.end()){
				//no entry, so make a new one
				user[status.displayName]=now;
			}else if(statusEntry->second+status.rate<=now){
				//apply status
				effects.push_back(status.apply());
				
				//TODO: consider making the loop iterators not-const and using those instead
				roomState[username]["statuses"][statusName]=status.getJson();
				
				if(status.numberOfTurns==0){
					toRemove.push_back(status.displayName);
				}
				
				statusEntry->second=now;
			}
		}
		
		effectReport[username]=effects;
		statusesCured[username]=toRemove;
	}
	
	//reporting must be done after iteration or we risk dropping users while we're in the middle of iterating over them
	for(const auto& [username,effects]:effectReport){
		string statusChangeText;
		if(effects.size()){
			statusChangeText=getStatusChangeText(username,effects);
		}
		
		//TODO: try to replace this all with applyStatusChanges
		int hp=roomState[username]["hp"];
		bool reportHp=false;
		for(const auto& effect:effects){
			hp-=effect.damage;
			reportHp|=(effect.damage!=0);
		}
	
		if(hp<=0){
			statusChangeText+=username+" has run out of hp";
		}else if(reportHp){
			statusChangeText+=username+" now has "+to_string(hp)+"hp left";
		}
	
		if(statusChangeText.length()){
			sendMessage(statusChangeText);
		}
		
		string statusExpiredMessage;
		for(const auto& name:statusesCured[username]){
			statusExpiredMessage+=username+" has been cured of "+name+"\n";
			roomState[username]["statuses"].erase(name);
		}
		
		if(statusExpiredMessage.length()){
			sendMessage(statusExpiredMessage);
		}
		
		updateUserHp(username,hp);
	}
	
	writeState();
	
	return true;
}

static inline tuple<vector<StatusResult>,vector<string>> applyPerPostStatusEffectsForUser(nlohmann::json& user){
	vector<StatusResult> effects;
	vector<string> toRemove;
	for(auto& [statusName,statusJson]:user["statuses"].items()){
		Status status(statusJson,statusName);
		
		//cout<<"checking status "<<status.displayName<<endl;
		
		if(status.applyEveryPost || status.numberOfTurns>0){
			//cout<<"status was applied"<<endl;
			effects.push_back(status.apply());
		}
		
		if(status.numberOfTurns==0){
			toRemove.push_back(status.displayName);
		}
		
		statusJson=status.getJson();
	}
	
	return std::tie(effects,toRemove);
}
string GameRoom::applyStatusChanges(const string& username,const vector<StatusResult>& effects,const vector<string>& toRemove){
	auto& user=roomState[username];
	
	string statusChangeText;
	if(effects.size()){
		statusChangeText=getStatusChangeText(username,effects);
	}
	
	int hp=user["hp"];
	bool reportHp=false;
	for(const auto& effect:effects){
		hp-=effect.damage;
		reportHp|=(effect.damage!=0);
	}
	updateUserHp(username,hp);
	
	if(hp<=0){
		statusChangeText+=username+" has run out of hp";
		return statusChangeText;
	}else if(reportHp){
		statusChangeText+=username+" now has "+to_string(user["hp"])+"hp left";
	}
	
	string statusExpiredMessage;
	for(const auto& name:toRemove){
		statusExpiredMessage+=username+" has been cured of "+name+"\n";
		user["statuses"].erase(name);
	}
	
	if(statusChangeText.length() && statusExpiredMessage.length()){
		statusChangeText+='\n';
	}
	statusChangeText+=statusExpiredMessage;
	
	return statusChangeText;
}

bool GameRoom::CheckMessage(const Bots::Message m){
	if(m.roomId!=roomId || !isPlaying(m.username)){
		return true;
	}
	
	//cout<<"checking message in game room"<<endl;
	
	unique_lock l(roomStateLock);
	
	auto& user=roomState[m.username];
	
	const auto [effects,toRemove]=applyPerPostStatusEffectsForUser(user);
	
	const auto statusChangeText=applyStatusChanges(m.username,effects,toRemove);
	
	if(statusChangeText.length()){
		sendMessage(statusChangeText);
	}
	
	writeState();
	
	return true;
}

static string getTextForStatus(const Status& status){
	string text;
	
	const string takeDamageText="recieve "+to_string(status.damage)+" damage";
	const string healDamageText="heal "+to_string(status.damage).substr(1)+" damage";
	
	const string damageText=status.damage.negate?healDamageText:takeDamageText;
	const string rateText="every "+to_string(status.rate.count())+" seconds";
	const string everyPostText="every post";
	const string turnsText="up to "+to_string(status.numberOfTurns)+" times";
	const string negateText="a "+to_string((int)(status.chanceToNegatePost*100))+"% chance to fail actions";
	
	//if the status can do damage, report how
	if(status.damage.count>0 && status.damage.sides>0){
		if(status.applyEveryPost){
			text+=damageText+" ";
			
			if(status.rate.count()>0){
				text+=rateText+" or ";
			}
			
			text+=everyPostText;
			
			if(status.numberOfTurns>0){
				text+=", "+turnsText;
			}
			
			text+='\n';
		}else if(status.rate.count()>0){
			text+=damageText+" "+rateText;
			
			if(status.numberOfTurns>0){
				text+=" "+turnsText;
			}
			
			text+='\n';
		}
	}
	
	if(status.chanceToNegatePost>0.0){
		text+=negateText;
		
		if(status.numberOfTurns>0){
			text+=" "+turnsText;
		}
	}
	
	return text;
}

std::string GameRoom::applyStatus(std::string username,std::string status){
	auto& user=roomState[username];
	const auto& statusEffect=availableStatuses.at(status);
	
	if(allowStacking && user["statuses"].contains(status)){
		//cout<<"Adding a new stack"<<endl;
		
		Status existingStatus(user["statuses"][status],status);
		existingStatus.addStack(statusEffect);
		
		user["statuses"][status]=existingStatus.getJson();
		
		return username+" has received an additional stack of "+status+" and will now receive the following effects:\n\n"+getTextForStatus(existingStatus);
	}else{
		//cout<<"Adding status"<<endl;
		
		user["statuses"][status]=statusEffect.getJson();
		
		return username+" now has "+status+" and will receive the following effects:\n\n"+getTextForStatus(statusEffect);
	}
}

void GameRoom::addStatus(std::string username,std::string status){
	unique_lock l(roomStateLock);
	
	//cout<<"Adding "<<status<<" to "<<username<<endl;
	
	if(availableStatuses.find(status)==availableStatuses.end()){
		sendMessage("Cannot add status "+status);
		return;
	}
	
	if(!isPlaying(username)){
		sendMessage(username+" is not currently playing");
		return;
	}
	
	//cout<<roomState<<endl;
	sendMessage(applyStatus(username,status));
	
	writeState();
}

bool GameRoom::userHasStatus(string username,string status){
	return isPlaying(username) && roomState[username]["statuses"].contains(status);
}
void GameRoom::removeStatus(std::string username,std::string status){
	unique_lock l(roomStateLock);
	
	if(userHasStatus(username,status)){
		roomState[username]["statuses"].erase(status);
		
		sendMessage(username+" has been cured of "+status);
	}
	
	writeState();
}
void GameRoom::setHp(std::string username,int value){
	unique_lock l(roomStateLock);
	
	updateUserHp(username,value);
	sendMessage("HP of "+username+" is now "+to_string(value));
	
	writeState();
}
void GameRoom::initializePlayer(std::string username){
	unique_lock l(roomStateLock);
	//cout<<"initializing: "<<username<<endl;
	
	if(isPlaying(username)){
		roomState.erase(username);
	}
	
	roomState[username]=nlohmann::json::object();
	
	const auto hp=hpDice.roll().sum;
	updateUserHp(username,hp);
	
	roomState[username]["statuses"]=nlohmann::json::object();
	
	sendMessage("Player "+username+" has been initalized with "+to_string(hp)+"hp");
	
	writeState();
}

void GameRoom::playerLeave(std::string username){
	if(hardcore){
		sendMessage("Leaving the game is not permitted in hardcore rooms");
		return;
	}
	
	removePlayer(username);
	sendMessage(username+" is no longer playing");
}

std::vector<std::string> GameRoom::getPlayers(){
	vector<string> players;
	for(const auto& [user,data]:roomState.items()){
		players.push_back(user);
	}
	
	return players;
}

vector<string> GameRoom::getAvailableActions(){
	vector<string> actions;
	for(const auto& action:availableActions){
		actions.push_back(action.first);
	}
	
	return actions;
}

void GameRoom::performAction(std::string actionName, std::string username, std::string target){
	unique_lock l(roomStateLock);
	
	//determine if action exists
	const auto actionItr=availableActions.find(actionName);
	if(actionItr==availableActions.end()){
		sendMessage(actionName+" is not available");
		return;
	}
	
	const auto& action=actionItr->second;
	
	//if the action does not allow self use, a target must be specified
	if(!action.self && target.length()==0){
		sendMessage(actionName+" requires a target");
		return;
	}
	
	//if the user is playing, and the action affects the user, apply effects to the user
	string userReport;
	if(isPlaying(username)){
		auto& user=roomState[username];
		const auto [effects,toRemove]=applyPerPostStatusEffectsForUser(user);
		
		if(effects.size() || toRemove.size()){
			const auto statusChangeReport=applyStatusChanges(username,effects,toRemove);
			userReport="While using "+actionName+", "+username+" has received the following effects:\n"+statusChangeReport;
			
			bool postNegated=false;
			for(const auto& effect:effects){
				if(effect.negateAction){
					postNegated=true;
					break;
				}
			}
			
			//if the user has died from this, or the action was negated, there is no further work to be done and their action has failed
			if(!isPlaying(username) || postNegated){
				sendMessage(userReport);
				writeState();
				return;
			}
		}
		
		writeState();
	}
	
	//TODO: if the action only allows self use, but a target is specified, maybe give a warning or reminder?
	
	//determine who is recieving the action
	string reciever;
	if(action.self && action.others){
		reciever=target.length()?target:username;
	}else if(action.self){
		reciever=username;
	}else if(action.others){
		reciever=target;
	}else{
		sendMessage(actionName+" is not able to be used on self or others, probably a config error");
		return;
	}
	
	//action recipient must be a valid user
	if(!isPlaying(reciever)){
		sendMessage(reciever+" is not currently playing");
		return;
	}
	
	auto& user=roomState[reciever];
	string report;

	//if an action requires a status to be in place, make sure it can happen
	vector<string> missingStatus;
	for(const auto& status:action.requiredStatus){
		if(!userHasStatus(reciever,status)){
			missingStatus.push_back(status);
		}
	}

	if(missingStatus.size()){
		if(userReport.length()){
			report=userReport+"\n\n";
		}
		
		sendMessage(report+"The action "+actionName+" failed because "+reciever+" was missing: "+join(missingStatus,", "));
		return;
	}
	
	//actually do the action
	auto result=action.act();
	
	//apply damage/healing
	const int newHp=user["hp"].get<int>()-result.damage;
	if(result.damage>0){
		report+=reciever+" received "+to_string(result.damage)+" damage from "+result.source+"\n";
	}else if(result.damage<0){
		report+=reciever+" healed "+to_string(-result.damage)+" hp from "+result.source+"\n";
	}
	
	updateUserHp(reciever,newHp);
	
	if(newHp<=0){
		if(userReport.length()){
			report=userReport+"\n"+report;
		}
		
		sendMessage(report+reciever+" has run out of hp");
		writeState();
		return;
	}
	
	if(result.damage!=0){
		report+='\n'+reciever+" now has "+to_string(newHp)+" hp\n\n";
	}
	
	//report on any cured statuses
	bool addNewLineAfterCureReport=false;
	for(const auto& status:result.cured){
		if(userHasStatus(reciever,status)){
			user["statuses"].erase(status);
			
			report+=reciever+" has been cured of "+status+" by "+actionName+"\n";
			addNewLineAfterCureReport=true;
		}
	}
	
	for(const auto& status:result.failedCure){
		if(userHasStatus(reciever,status)){
			report+=actionName+" failed to cure "+status+" of "+reciever+"\n";
			addNewLineAfterCureReport=true;
		}
	}
	
	if(addNewLineAfterCureReport){
		report+='\n';
	}
	
	//report on any new statuses
	if(result.applied.size()){
		report+=actionName+" has caused the following:"+'\n';
	}
	
	for(const auto& status:result.applied){
		report+=applyStatus(reciever,status)+'\n';
	}
	
	for(const auto& status:result.failedApply){
		report+=actionName+" failed to apply "+status+" to "+reciever+"\n";
	}
	
	if(report.length()==0){
		report=reciever+" tried to "+actionName+" but it did nothing";
	}
	
	if(userReport.length()){
		report=userReport+"\n"+report;
	}
	
	writeState();
	sendMessage(report);
}
