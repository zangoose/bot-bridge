/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2020  zangoose <email>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) version 3, or any
 * later version accepted by the membership of KDE e.V. (or its
 * successor approved by the membership of KDE e.V.), which shall
 * act as a proxy defined in Section 6 of version 3 of the license.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "gameobjects.h"
#include <regex>
#include <algorithm>
#include <iostream>
#include <random>
#include <cmath>
using namespace std;

// trim from start (in place)
static inline void ltrim(string &s) {
	s.erase(s.begin(), find_if(s.begin(), s.end(), [](unsigned char ch) {
		return !isspace(ch);
	}));
}

// trim from end (in place)
static inline void rtrim(string &s) {
	s.erase(find_if(s.rbegin(), s.rend(), [](unsigned char ch) {
		return !isspace(ch);
	}).base(), s.end());
}

// trim from both ends (in place)
static inline string trim(string s) {
	ltrim(s);
	rtrim(s);
	
	return s;
}

string toLower(string s){
	transform(s.begin(), s.end(), s.begin(), ::tolower);
	return s;
}

static const regex diceMatch("-?([0-9]+)\\s*d\\s*([0-9]+)");
Dice::Dice(string type){
	type=toLower(trim(type));
	
	smatch matches;
	
	if(regex_search(type, matches, diceMatch)) {
		negate=(matches[0].str()[0]=='-');
		
		count=stoi(matches[1]);
		sides=stoi(matches[2]);
	} else {
		throw runtime_error("Could not make dice from string:"+type);
	}
	
	//cout<<"made "<<count<<" dice with "<<sides<<" sides, negated: "<<negate<<endl;
}
Dice::Dice(const int count,const int sides,const bool negate):count(count),sides(sides),negate(negate){}

static random_device generator;
DiceRoll Dice::roll() const{
	DiceRoll result;
	result.rolls.resize(count);
	
	int total=0;
	const int sign=negate?-1:1;
	
	uniform_int_distribution rand(1,sides);
	
	for(int i=0;i<count;i++){
		const auto roll=sign*rand(generator);
		result.rolls[i]=roll;
		total+=roll;
	}
	
	result.sum=total;
	return result;
}

std::string to_string(const Dice& dice){
	string negative=dice.negate?"-":"";
	return negative+to_string(dice.count)+"d"+to_string(dice.sides);
}

template<typename T>
T getJsonValueOrDefault(const nlohmann::json source,const std::string key,const T defaultValue=T()){
	if(source.contains(key)){
		return source[key];
	}
	return defaultValue;
}

static regex timeMatch("([0-9]+)([smh])");
chrono::seconds parseDurationString(string duration){
	chrono::seconds t(0);
	for(auto itr=sregex_iterator(duration.begin(),duration.end(),timeMatch);itr!=sregex_iterator();++itr){
		//cout<<(*itr)[0]<<" "<<(*itr)[1]<<" "<<(*itr)[2]<<endl;
		const auto value=stoi((*itr)[1].str());
		const auto suffix=(*itr)[2].str()[0];
		
		//cout<<value<<" "<<suffix<<endl;
		
		switch(suffix){
			case 's':
				t+=chrono::seconds(value);
				break;
			case 'm':
				t+=chrono::minutes(value);
				break;
			case 'h':
				t+=chrono::hours(value);
				break;
			//TODO: c++20 introduces chrono::days, chrono::weeks, chrono::months, chrono::years
			//case 'd':
		}
	}
	
	return t;
}

Status::Status(nlohmann::json data,string name):
	displayName(name),
	damage(getJsonValueOrDefault(data,"damage","0d0"s)),
	rate(parseDurationString(getJsonValueOrDefault(data,"every","0s"s))),
	applyEveryPost(getJsonValueOrDefault(data,"applyEveryPost",false)),
	chanceToNegatePost(getJsonValueOrDefault(data,"discardChance",0.0)),
	numberOfTurns(getJsonValueOrDefault(data,"turns",-1))
{}

void Status::addStack(const Status& stack){
	//TODO: stack dice damage?
	
	//take the mean of the rates, average them, and decrease the result a bit
	if(rate.count()>0 && stack.rate.count()>0){
		auto newRate=(rate.count()+stack.rate.count())/2;
		newRate=newRate/3;
		rate=chrono::seconds(max(newRate,1l));
	}
	
	//applyEveryPost does not get stacked
	
	chanceToNegatePost=1-(1-chanceToNegatePost)*(1-stack.chanceToNegatePost);
	
	if(numberOfTurns>0 && stack.numberOfTurns>0){
		numberOfTurns+=stack.numberOfTurns;
	}
}

static uniform_real_distribution<double> rnd(0.0,1.0);
StatusResult Status::apply(){
	StatusResult result;
	result.damage=damage.roll().sum;
	
	result.source=displayName;
	
	result.negateAction=chanceToNegatePost>rnd(generator);
	
	if(numberOfTurns>0){
		--numberOfTurns;
	}
	
	return result;
}

nlohmann::json Status::getJson() const{
	nlohmann::json result=nlohmann::json::object();
	
	result["damage"]=to_string(damage);
	result["every"]=to_string(rate.count())+"s";
	result["applyEveryPost"]=applyEveryPost;
	result["discardChance"]=chanceToNegatePost;
	result["turns"]=numberOfTurns;
	result["name"]=displayName;
	
	return result;
}

Action::Action(nlohmann::json data, std::string name):
	displayName(name),
	damage(getJsonValueOrDefault(data,"damage","0d0"s)),
	self(getJsonValueOrDefault(data,"self",true)),
	others(getJsonValueOrDefault(data,"others",true))
{
	if(data.contains("applies")){
		//TODO: check if status exists and give a warning and/or create a status object instead of a string
		for(const auto& [name,chance]:data["applies"].items()){
			possibleStatus[name]=chance;
		}
	}
	
	if(data.contains("cures")){
		//TODO: check if status exists and give a warning and/or create a status object instead of a string
		for(const auto& [name,chance]:data["cures"].items()){
			possibleCures[name]=chance;
		}
	}
	
	if(data.contains("requires")){
		//TODO: check if status exists and give a warning and/or create a status object instead of a string
		for(const auto& status:data["requires"]){
			requiredStatus.insert(status.get<string>());
		}
	}
}

ActionResult Action::act() const{
	ActionResult result;
	result.source=displayName;
	
	result.damage=damage.roll().sum;
	
	for(const auto& [status,chance]:possibleStatus){
		if(rnd(generator)<chance){
			result.applied.push_back(status);
		}else{
			result.failedApply.push_back(status);
		}
	}
	
	for(const auto& [status,chance]:possibleCures){
		if(rnd(generator)<chance){
			result.cured.push_back(status);
		}else{
			result.failedCure.push_back(status);
		}
	}
	
	return result;
}
