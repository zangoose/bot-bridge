#include <iostream>
#include <string>
#include <curl/curl.h>
#include <nlohmann/json.hpp>
#include "botbridge.h"
#include "setuphelpers.h"
using namespace std;

int main(int argc, char **argv) {
    curl_global_init(CURL_GLOBAL_SSL);
	
	//TODO: file name should come from input, this should be a default
	const auto configStr=injestJsonFile("config.json").str();
	//cout<<configStr<<endl;
	auto config=nlohmann::json::parse(configStr);
	//cout<<config<<endl;
	
	BotBridge bridge;
	
	if(config.contains("database")){
		Bots::setDatabasePath((string)config["database"]);
	}else{
		Bots::setDatabasePath(configPath());
	}
	
	if(config.contains("logging")){
		bridge.enableLogging=config["logging"];
	}
	
	if(config.contains("greeting")){
		bridge.greeting=config["greeting"];
	}
	
	if(config.contains("bridges")){
		setupBridges(config["bridges"],bridge);
	}
	
	if(config.contains("external")){
		setupExternal(config["external"],bridge);
	}
	
	if(config.contains("bots")){
		setupBots(config["bots"],bridge);
	}
	
	if(config.contains("games")){
		bridge.initializeGames(config["games"]);
	}
	
	bridge.waitForAllBotsToFinish();
	
	curl_global_cleanup();
    return 0;
}
