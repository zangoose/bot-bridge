/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2020  Jack Phoebus <email>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "botbridge.h"
#include <iostream>
using namespace std;

BotBridge::BotBridge(){
}

BotBridge::~BotBridge(){
	for(auto bot:bots){
		delete bot;
	}
	
	for(auto game:games){
		delete game.second;
	}
}

void BotBridge::addBot(Bots::Bot* bot){
	bots.push_back(bot);
	addAllCommandsTo(bot);
	addAllResponseHandlersTo(bot);
	bot->start();
}

void BotBridge::waitForAllBotsToFinish(){
	for(auto bot:bots){
		bot->waitUntilDone();
	}
}

static void printBridges(const std::unordered_map<std::string,std::unordered_set<std::string>>& bridges){
	cout<<"Brides:"<<endl;
	for(const auto& bridge:bridges){
		cout<<"{'"<<bridge.first<<"':";
		for(const auto& connection:bridge.second){
			cout<<"'"<<connection<<"' ";
		}
		cout<<endl;
	}
}

void BotBridge::linkRooms(const vector<string>& rooms){
	for(const auto& room:rooms){
		auto [itr,_]=bridges.try_emplace(room);
		itr->second.insert(rooms.begin(),rooms.end());
		itr->second.erase(room);
	}
	printBridges(bridges);
}

void BotBridge::addExternalProcess(externalProcessArgs proc,string command){
	if(command.length()){
	}else{
		externalProcesses.push_back(proc);
	}
}

void BotBridge::addGameToRoom(BotBridge& bridge,string room,bool hardcore,nlohmann::json game){
	if(games.find(room)!=games.end()){
		throw std::runtime_error("The room "+room+" has multiple games assigned to it");
	}
	
	//cout<<"Making game room for "<<room;
	
	auto gameRoom=new GameRoom(game,room);
	gameRoom->hardcore=hardcore;
	
	games[room]=gameRoom;
	
	gameRoom->wireEvents();
}

void BotBridge::initializeGames(nlohmann::json config){
	for(auto& game:config){
		cout<<game<<endl;
		
		if(game.contains("rooms")){
			for(const auto& room:game["rooms"]){
				addGameToRoom(*this,room,false,game);
			}
		}
		
		if(game.contains("hardcore")){
			for(const auto& room:game["hardcore"]){
				addGameToRoom(*this,room,true,game);
			}
		}
	}
}
