#include <iostream>
#include <fstream>
#include <regex>
#include <filesystem>
#include "setuphelpers.h"
using namespace std;

static string dataDirectory(){
#if _WIN32 || _WIN64
	return getenv("APPDATA");//TODO: test
#elif __APPLE__ || __MACH__
	return getenv("HOME")+"/Library/Application Support"s;//TODO: test
#else
	//unix/linux/bsd
	return getenv("HOME")+"/.config"s;
#endif
}

string configPath(){
	return filesystem::path(dataDirectory())/".bot-bridge";
}

stringstream injestJsonFile(string fname){
	ifstream in(fname);
	
	if(!in.is_open()){
		//TODO: error
		cout<<fname<<" could not be opened"<<endl;
	}
	
	string line;
	stringstream data;
	
	regex isPoundComment("^\\s*#");
	regex isSlashComment("^\\s*//");
	while(getline(in,line)){
		smatch match;
		if(!regex_search(line,match,isPoundComment) && !regex_search(line,match,isSlashComment)){
			data<<line<<endl;
		}
	}
	
	return data;
}

void setupBots(const nlohmann::json& botsConfig, BotBridge& bridge){
	for(const auto& botJSON:botsConfig){
		//cout<<botJSON<<endl;
		
		if(!botJSON.contains("type")){
			cout<<"WARN: bot is missing type"<<endl;
			continue;
		}
		
		auto type=botJSON["type"];
		if(!type.is_null()){
			Bots::Bot* bot=nullptr;
			const string typestr=type;
			
			const auto label=getJsonValueOrDefault<string>(botJSON,"label");
			
			//TODO: case insensitivity
			if(typestr=="telegram"){
				auto token=botJSON["token"].get<string>();
				const auto owner=getJsonValueOrDefault<string>(botJSON,"owner");
				
				//cout<<token<<endl<<owner<<endl;
				bot=new Bots::TelegramBot(label,token,owner);
				//cout<<"making telegram bot: "<<botJSON<<endl;
				
				Bots::wipeString(token);
			}else if(typestr=="matrix"){
				const auto owner=getJsonValueOrDefault<string>(botJSON,"owner");
				const auto username=botJSON["username"].get<string>();
				const auto domain=botJSON["domain"].get<string>();
				auto password=botJSON["password"].get<string>();
				const auto deviceId=getJsonValueOrDefault<string>(botJSON,"device_id");
				const auto deviceDisplayName=getJsonValueOrDefault<string>(botJSON,"display_name");
				
				bot=new Bots::MatrixBot(label,username,password,domain,deviceId,deviceDisplayName,owner);
				
				Bots::wipeString(password);
			}else{
				//TODO: unknown type
			}
			
			if(bot){
				bridge.addBot(bot);
			}
		}
	}
}

void setupBridges(const nlohmann::json& bridgesConfig, BotBridge& bridge){
	for(const auto& items:bridgesConfig){
		vector<string> b=items;
		bridge.linkRooms(b);
	}
}


void setupExternal(const nlohmann::json& externalConfig, BotBridge& bridge){
	for(const auto& external:externalConfig){
		externalProcessArgs args;
		
		args.executable=external["executable"];
		args.args=getJsonValueOrDefault<vector<string>>(external,"args");
		args.whitelist=getJsonValueOrDefault<vector<string>>(external,"whitelist");
		args.blacklist=getJsonValueOrDefault<vector<string>>(external,"blacklist");
		args.outputIsReply=getJsonValueOrDefault(external,"outputIsReply",false);
		args.passMessageToStdin=getJsonValueOrDefault(external,"passMessageToStdin",false);
		
		const string command=getJsonValueOrDefault<string>(external,"command","");
		bridge.addExternalProcess(args,command);
	}
}
