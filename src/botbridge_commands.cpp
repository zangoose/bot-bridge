#include "botbridge.h"
#include <iostream>
#include <unordered_map>
#include <functional>
#include <regex>
using namespace std;

void BotBridge::addAllCommandsTo(Bots::Bot* bot){
	const static unordered_map<string,function<bool(BotBridge*,Bots::Message)>> commandMapping{
		{"start",&BotBridge::cmd_start},
		{"test",&BotBridge::cmd_test},
		{"id",&BotBridge::cmd_id},
		{"dice",&BotBridge::cmd_dice},
		{"roll",&BotBridge::cmd_dice},
		{"status",&BotBridge::cmd_game_status},
		{"play",&BotBridge::cmd_game_join},
		{"leave",&BotBridge::cmd_game_leave},
		{"action",&BotBridge::cmd_game_action},
		{"get",&BotBridge::cmd_game_get}
	};
	
	for(const auto& [command,action]:commandMapping){
		bot->setCommandHandler(command,bind(action,this,placeholders::_1));
	}
}

bool BotBridge::cmd_start(const Bots::Message m){
	if(greeting.size()){
		Bots::Message response;
		response.roomId=m.roomId;
		response.body=greeting;
		
		m.origin->sendMessage(response);
	}
	
	return true;
}

bool BotBridge::cmd_test(const Bots::Message m){
	Bots::Message response;
	response.roomId=m.roomId;
	response.body="Test!";
	
	cout<<"sending "<<response.body<<" to "<<response.roomId<<endl;
	
	m.sendReply(response);
	
	return true;
}

bool BotBridge::cmd_id(const Bots::Message m){
	m.sendReply(m.roomId);
	
	return true;
}

template<typename T>
static inline string join(const string& segment,T begin,const T end){
	if(begin==end){
		return "";
	}
	
	//string result=std::to_string(*begin);
	stringstream result;
	result<<*begin++;
	
	while(begin!=end){
		result<<segment;
		result<<*begin++;
	}
	
	return result.str();
}

template<typename T>
static inline string join(const string& segment,const valarray<T>& begin){
	return join(segment,&begin[0],&begin[begin.size()]);
}

static constexpr int DICE_MAX_COUNT=100;
static constexpr int DICE_MAX_SIDES=1000;

bool BotBridge::cmd_dice(const Bots::Message m){
	Bots::Message response;
	response.roomId=m.roomId;
	
	try{
		auto dice=Dice(m.body);
		if(dice.count>DICE_MAX_COUNT){
			response.body="Number of dice is limited to "+to_string(DICE_MAX_COUNT);
		}else if(dice.sides>DICE_MAX_SIDES){
			response.body="Number of sides is limited to "+to_string(DICE_MAX_SIDES);
		}else{
			auto roll=dice.roll();
			
			if(dice.count<=10){
				response.body=join<int>(" + ",roll.rolls)+"\n"s;
			}
			
			response.body+="you rolled: "+to_string(roll.sum);
		}
		
		m.sendReply(response);
	}catch(exception e){
		//nothing to do, just ignore this one
	}
	
	return true;
}

GameRoom* BotBridge::getGameRoomForMessage(const Bots::Message& m){
	string roomName=m.origin->label()+":"+m.roomId;
	auto game=games.find(roomName);
	
	if(game==games.end()){
		return nullptr;
	}
	
	return game->second;
}

static const regex statusCommandRegex("(add|remove)\\s+(\\w+)\\s+@?(\\w+)",regex::ECMAScript|regex::icase);
static const regex resetCommandRegex("(reset|clear)\\s+@?(\\w+)",regex::ECMAScript|regex::icase);
bool BotBridge::cmd_game_status(const Bots::Message m){
	smatch matches;
	
	//if there is no game for this room, dont try
	auto game=getGameRoomForMessage(m);
	
	if(!game){
		m.sendReply("No game running in this room");
		
		return true;
	}
	
	//if the user is not privledged, dont let them use this command
	if(!game->isPrivledgedUser(m.username)){
		m.sendReply("You are not allowed to do that");
		
		return true;
	}
	
	//checks are good, parse the command
	if(regex_search(m.body,matches,statusCommandRegex)) {
		const auto action=toLower(matches[1]);
		const auto status=toLower(matches[2]);
		const auto user=matches[3].str();
		
		//m.sendReply(action+" "+status+" for "+user);
		
		if(action=="add"){
			game->addStatus(user,status);
		}else if(action=="remove"){
			game->removeStatus(user,status);
		}
	}else if(regex_search(m.body,matches,resetCommandRegex)){
		const auto action=toLower(matches[1]);
		const auto user=matches[2].str();
		
		if(action=="reset"){
			game->initializePlayer(user);
		}else if(action=="clear"){
			//TODO
		}
	}else{
		m.sendReply("Failed to parse arguments: "+m.body);
	}
	
	return true;
}
bool BotBridge::cmd_game_set(const Bots::Message m){
	//TODO: what is there to set other than hp?
	return true;
}
bool BotBridge::cmd_game_join(const Bots::Message m){
	auto game=getGameRoomForMessage(m);
	
	if(!game){
		m.sendReply("No game running in this room");
		
		return true;
	}
	
	if(!game->isPlaying(m.username)){
		game->initializePlayer(m.username);
	}else{
		m.sendReply(m.username+" is already playing");
	}
	
	return true;
}
bool BotBridge::cmd_game_leave(const Bots::Message m){
	auto game=getGameRoomForMessage(m);
	
	if(!game){
		m.sendReply("No game running in this room");
		
		return true;
	}
	
	game->playerLeave(m.username);
	
	return true;
}

static const regex actionCommandRegex("([\\w-]+)\\s*@?(\\w+)?",regex::ECMAScript|regex::icase);
bool BotBridge::cmd_game_action(const Bots::Message m){
	auto game=getGameRoomForMessage(m);
	
	if(!game){
		m.sendReply("No game running in this room");
		
		return true;
	}
	
	smatch matches;
	if(regex_search(m.body,matches,actionCommandRegex)){
		const auto action=toLower(matches[1]);
		const auto target=matches[2].str();
		
		game->performAction(action,m.username,target);
	}else{
		//TODO: report on possible actions
	}
	
	return true;
}

static const regex getCommandRegex("(\\w+)",regex::ECMAScript|regex::icase);
bool BotBridge::cmd_game_get(const Bots::Message m){
	auto game=getGameRoomForMessage(m);
	
	if(!game){
		m.sendReply("No game running in this room");
		
		return true;
	}
	
	smatch matches;
	if(regex_search(m.body,matches,actionCommandRegex)){
		const auto what=toLower(matches[1]);
		
		if(what=="actions"){
			const auto actions=game->getAvailableActions();
			
			m.sendReply("Available actions: "+join(", ",actions.begin(),actions.end()));
		}else if(what=="players"){
			const auto players=game->getPlayers();
			
			m.sendReply("Currently playing: "+join(", ",players.begin(),players.end()));
		}else{
			m.sendReply("Unknown attribute: "+what);
		}
	}else{
		m.sendReply("Failed to parse arguments: "+m.body);
	}
	
	return true;
}


