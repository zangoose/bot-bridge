/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2020  zangoose <email>
 * 
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) version 3, or any
 * later version accepted by the membership of KDE e.V. (or its
 * successor approved by the membership of KDE e.V.), which shall
 * act as a proxy defined in Section 6 of version 3 of the license.
 * 
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef GAMEOBJECTS_H
#define GAMEOBJECTS_H

#include <string>
#include <valarray>
#include <chrono>
#include <mutex>
#include <set>

#include "bot.h"
#include "message.h"

//TODO: need to do something about all of these re-used helpers
std::string toLower(std::string s);

struct DiceRoll{
	std::valarray<int> rolls;
	int sum;
};

class Dice{
public:
	int count,sides;
	bool negate;
	
	Dice(std::string type);
	Dice(const int count,const int sides,const bool negate=false);
	
	DiceRoll roll() const;
};

std::string to_string(const Dice& dice);

struct Status;
struct StatusResult{
	int damage;
	bool negateAction;
	std::string source;
};

struct Status{
	std::string displayName;
	Dice damage;
	std::chrono::seconds rate;
	bool applyEveryPost;
	double chanceToNegatePost;
	int numberOfTurns;
	
	Status(nlohmann::json data,std::string name);
	
	void addStack(const Status& stack);
	nlohmann::json getJson() const;
	
	StatusResult apply();
};

struct ActionResult{
	int damage;
	std::vector<std::string> applied;
	std::vector<std::string> cured;
	std::vector<std::string> failedApply;
	std::vector<std::string> failedCure;
	std::string source;
};

struct Action{
	std::string displayName;
	Dice damage;
	std::unordered_map<std::string,double> possibleStatus;
	std::unordered_map<std::string,double> possibleCures;
	std::set<std::string> requiredStatus;
	bool self,others;
	
	Action(nlohmann::json data,std::string name);
	
	ActionResult act() const;
};

/*TODO:
set up database logging - done
add event handling for user join - done
add parsing for game config - done
finish this class - done
add command handlers
add user kick function
*/

class GameRoom{
private:
	std::unordered_map<std::string,Status> availableStatuses;
	std::unordered_map<std::string,Action> availableActions;
	Bots::Bot* bot;
	std::string roomId;
	std::filesystem::path stateFile;
	
	bool eventsWired=false;
	
	Dice hpDice;
	bool allowStacking=false;
	
	nlohmann::json roomState=nlohmann::json::object();
	std::mutex roomStateLock;
	
	std::unordered_map<
		std::string,
		std::unordered_map<
			std::string,
			std::chrono::steady_clock::time_point
		>
	> statusLastAppliedTimestamp;
	
	std::set<std::string> privledgedUsers;
	
	void writeState();
	bool onUserJoin(ChatEvent e);
	
	void updateUserHp(std::string username,int value);
	void removePlayer(std::string username);
	void sendMessage(std::string message);
	
	std::string getStatusChangeText(std::string username,const std::vector<StatusResult>& changes);
	std::string applyStatus(std::string username,std::string status);
	std::string applyStatusChanges(const std::string& username,const std::vector<StatusResult>& effects,const std::vector<std::string>& toRemove);
	
	bool userHasStatus(std::string username,std::string status);
public:
	bool hardcore=false;
	
	GameRoom(const nlohmann::json& data,std::string room);
	
	//TODO: while not technically necessary, this is convenient for controlling when things start working, evaluate cost/benefit
	void wireEvents();
	
	bool isPrivledgedUser(std::string username);
	bool isPlaying(std::string username);
	
	bool CheckMessage(const Bots::Message m);
	bool RunTimedEvents();
	
	void addStatus(std::string username,std::string status);
	void removeStatus(std::string username,std::string status);
	void setHp(std::string username,int value);
	void initializePlayer(std::string username);
	
	void playerLeave(std::string username);
	std::vector<std::string> getPlayers();
	
	std::vector<std::string> getAvailableActions();
	void performAction(std::string action,std::string username,std::string target);
};

#endif // GAMEOBJECTS_H
