/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2020  Jack Phoebus <email>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SETUPHELPERS_H
#define SETUPHELPERS_H

#include <string>
#include <sstream>
#include <nlohmann/json.hpp>
#include "botbridge.h"

std::string configPath();

std::stringstream injestJsonFile(std::string fname);

template<typename T>
T getJsonValueOrDefault(const nlohmann::json source,const std::string key,const T defaultValue=T()){
	if(source.contains(key)){
		return source[key];
	}
	return defaultValue;
}

void setupBots(const nlohmann::json& botsConfig, BotBridge& bridge);
void setupBridges(const nlohmann::json& bridgesConfig, BotBridge& bridge);
void setupExternal(const nlohmann::json& externalConfig, BotBridge& bridge);
#endif // SETUPHELPERS_H
