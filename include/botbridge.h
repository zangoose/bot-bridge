/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2020  Jack Phoebus <email>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOTBRIDGE_H
#define BOTBRIDGE_H

#include <string>
#include <unordered_set>
#include <unordered_map>
#include <vector>
#include "botcollection.h"
#include "gameobjects.h"

struct externalProcessArgs{
	std::string executable;
	std::vector<std::string> args,whitelist,blacklist;
	bool outputIsReply=false;
	bool passMessageToStdin=false;
};

class BotBridge{
private:
	std::vector<Bots::Bot*> bots;
	std::vector<externalProcessArgs> externalProcesses;
	std::unordered_map<std::string,std::unordered_set<std::string>> bridges;
	
	std::unordered_map<std::string,GameRoom*> games;
	
	void logToFileInCurrentDatabasePath(const Bots::Message m,std::string filename,std::string message);
	
	void addAllResponseHandlersTo(Bots::Bot* bot);
	bool echo(const Bots::Message m);
	bool brige(const Bots::Message m);
	bool log(const Bots::Message m);
	bool passToProcesses(const Bots::Message m);
	
	void passToProcess(const Bots::Message m, const externalProcessArgs proc);
	
	void addAllCommandsTo(Bots::Bot* bot);
	bool cmd_start(const Bots::Message m);
	bool cmd_test(const Bots::Message m);
	bool cmd_id(const Bots::Message m);
	
	void addGameToRoom(BotBridge& bridge,std::string room,bool hardcore,nlohmann::json game);
	GameRoom* getGameRoomForMessage(const Bots::Message& m);
	
	bool cmd_dice(const Bots::Message m);
	bool cmd_game_status(const Bots::Message m);
	bool cmd_game_set(const Bots::Message m);
	bool cmd_game_join(const Bots::Message m);
	bool cmd_game_leave(const Bots::Message m);
	bool cmd_game_action(const Bots::Message m);
	bool cmd_game_get(const Bots::Message m);
public:
	bool enableLogging=false;
	std::string greeting;
	
    BotBridge();
	~BotBridge();
	
	void addBot(Bots::Bot* bot);
	void waitForAllBotsToFinish();
	
	void linkRooms(const std::vector<std::string>& rooms);
	void addExternalProcess(externalProcessArgs proc,std::string command="");
	
	void initializeGames(nlohmann::json config);
};

#endif // BOTBRIDGE_H
