/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2020  Jack Phoebus <email>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "curlinstance.h"
#include <iostream>
#include <curl/curl.h>
using namespace std;

network_error::network_error(int err_code,std::string serverResponse,std::string errorMessage):
	runtime_error(errorMessage),
	code(err_code),
	response(serverResponse)
{
}


long network_error::Code() const{
	return code;
}

std::string network_error::Response() const{
	return response;
}


static size_t curlWriter(char* data,size_t size,size_t nmemb,stringstream* buffer){
	buffer->write(data,nmemb);
	
	return nmemb;
}

static int curl_dbg(CURL *handle,curl_infotype type,char *data,size_t size,void *userptr){
	cout<<"Dbg: "<<type<<": "<<data<<endl;
	return 0;
}

CurlInstance::CurlInstance(){
	unique_lock l(lock);
	curlHandle=curl_easy_init();
}
CurlInstance::~CurlInstance(){
	unique_lock l(lock);
	curl_easy_cleanup(curlHandle);
}

std::string CurlInstance::urlEncode(const std::string str){
	static CURL* c=nullptr;
	//NOTE:not sure if easy_escape is thread safe but it's probably fine...
	//it's also probably fine that this technically never gets cleaned up explicitly...
	if(!c){
		c=curl_easy_init();
	}
	
	auto encoded=curl_easy_escape(c,str.c_str(),str.size());
	
	string rval(encoded);
	curl_free(encoded);
	
	//NOTE: if c is made non-static, this is necessary
	//curl_easy_cleanup(c);
	
	return rval;
}

stringstream CurlInstance::makeRequest(const string& url,const nlohmann::json& args,const std::string& customHeader,const bool forcePost){
	//curl handles can only be used in one thread at a time
	unique_lock l(lock);
	
	//curl_easy_setopt(curlHandle,CURLOPT_VERBOSE,1);
	//curl_easy_setopt(curlHandle,CURLOPT_DEBUGFUNCTION,curl_dbg);
	
	curl_slist* headers = NULL;
		
	if(customHeader!=""){
		headers=curl_slist_append(headers,customHeader.c_str());
	}
	
	string postData;
	if(!args.is_null() || forcePost){
		headers=curl_slist_append(headers,"Accept: application/json");
		headers=curl_slist_append(headers,"Content-Type: application/json");
		headers=curl_slist_append(headers,"charsets: utf-8");
		
		//curl_easy_setopt(curlHandle,CURLOPT_POST,1L);
		
		if(args.is_null()){
			postData="{}";
		}else{
			postData=args.dump();
		}
		
		//cout<<"dump: "<<args<<endl;
		curl_easy_setopt(curlHandle,CURLOPT_POSTFIELDS,postData.c_str());
		curl_easy_setopt(curlHandle,CURLOPT_POSTFIELDSIZE,postData.size());
	}
	
	if(headers){
		curl_easy_setopt(curlHandle,CURLOPT_HTTPHEADER,headers);
	}
	
	curl_easy_setopt(curlHandle,CURLOPT_URL,url.c_str());
	
	//printf("setting data ptr\n");
	stringstream response;
	curl_easy_setopt(curlHandle,CURLOPT_WRITEFUNCTION,curlWriter);
	curl_easy_setopt(curlHandle,CURLOPT_WRITEDATA,&response);
	
	//printf("sending request\n");
	auto code=curl_easy_perform(curlHandle);
	
	long responseCode;
	curl_easy_getinfo(curlHandle,CURLINFO_RESPONSE_CODE,&responseCode);
	
	curl_slist_free_all(headers);
	curl_easy_reset(curlHandle);//TODO: only reset what's needed
	
	if(code!=CURLE_OK){
		//TODO:error happened, can it be handled without throwing?
		throw runtime_error("Curl error: "+to_string(code)+" from request: "+url);
	}
	
	//1xx codes are not part of http/1.0 and may not need handling
	//2xx codes mean everything was ok
	//3xx codes mean redirection handled, we can probably handle them here, TODO
	//4xx codes mean an error happened because of the client, we should throw
	//5xx codes mean an error happened because of the server, we should throw
	if(responseCode<200 || responseCode>=300){
		stringstream ss;
		ss<<"Bad response from server: "<<responseCode<<endl;
		ss<<"\trequest: "<<url<<endl;
		ss<<"\tresponse: "<<response.str()<<endl;
		throw network_error(responseCode,response.str(),ss.str());
	}
	
	//if we got a 200s code and still didnt get json back, something is up and needs a closer look
	if(response.str()[0]=='<'){
		cerr<<"Non-json response received: "<<response.str()<<endl;
	}
	
	return response;
}
