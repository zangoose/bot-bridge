/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2020  Jack Phoebus <email>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "telegrambot.h"
#include "curlinstance.h"
#include <algorithm>
#include <iostream>
#include <functional>
using namespace Bots;

static inline std::string toLower(std::string str){
	std::transform(str.begin(),str.end(),str.begin(),::tolower);
	return str;
}

nlohmann::json Bots::TelegramBot::makeApiRequest(const std::string method,nlohmann::json args){
	static const int retryLimit=3;
	static const auto retrySleepDuration=std::chrono::milliseconds(6000);
	
	const std::string requestUrl="https://api.telegram.org/bot"+token+"/"+method;
	//std::cout<<requestUrl<<std::endl;
	//std::cout<<args<<std::endl;
	CurlInstance curl;
	
	int attempts=1;
	while(true){
		try{
			auto response=curl.makeRequest(requestUrl,args);
			return nlohmann::json::parse(response);
		}catch(const network_error& err){
			//TODO: telegram can occasionally produce 502 (bad gateway) errors, retry after catching them
			//TODO: other error handling?
			if(err.Code()==400 || err.Code()==403){//bad request, this can happen if you try to, for example, reply to a deleted message, 403 can be caused by "bot was blocked by the user", TODO: make this a switch
				std::cerr<<err.what()<<std::endl;
				return nlohmann::json::parse(err.Response());
			}else{
				std::cout<<"Unhandled network error from telegram request:"
				<<"\tRequest: "<<requestUrl
				<<"\tCode: "<<err.Code()
				<<"\tException: "<<err.what()<<std::endl;
				
				if(attempts>=retryLimit){
					throw std::move(err);
				}
			}
		}catch(const std::runtime_error& err){
			std::cout<<"Telegram caught unhandled runtime error:"
			<<"\tRequest: "<<requestUrl
			<<"\tException: "<<err.what()<<std::endl;
			std::cout<<"Aborting..."<<std::endl;
			
			//TODO: retry indefinitely if this happens?  it was most likely caused by some temporary curl-related error, like internet cutting out, which could still take a couple minutes to recover
			if(attempts>=retryLimit){
				throw std::move(err);
			}
		}catch(const std::exception& e){
			std::cout<<"Telegram caught unhandled exception:"
			<<"\tRequest: "<<requestUrl
			<<"\tException: "<<e.what()<<std::endl;
			
			if(attempts>=retryLimit){
				throw std::move(e);
			}
		}
		
		std::cout<<"Retrying..."<<std::endl;
		++attempts;
		std::this_thread::sleep_for(retrySleepDuration);
	}
}

TelegramBot::TelegramBot(const std::string& label,const std::string& botToken,const std::string& botOwner):Bot(label), token(botToken), owner(toLower(botOwner)){
	auto response=makeApiRequest("getMe");
	
	if(!response.contains("result")){
		//TODO:error
	}
	std::cout<<"Telegram bot info: "<<response["result"]<<std::endl;
	
	username=response["result"]["username"];
}

TelegramBot::~TelegramBot(){
	wipeString(token);
}

static inline std::string getUsername(nlohmann::json json){
	if(json.contains("username")){
		return json["username"];
	}else if(json.contains("first_name")){
		return json["first_name"];
	}
	return json["id"];
}

bool TelegramBot::mainLoop(){
	auto messagesJSON=makeApiRequest("getUpdates",{{"offset",lastMessageId+1}});
	
	if(!messagesJSON.contains("result")){
		//TODO:error
	}
	//body,sender,roomId,messageId
	for(const auto& messageJSON:messagesJSON["result"]){
		//std::cout<<messageJSON<<std::endl;
		Message message;
		message.raw=messageJSON;
		
		if(messageJSON.contains("message")){
			const auto innerMessage=messageJSON["message"];
			//std::cout<<innerMessage<<std::endl;
			
			const auto from=innerMessage["from"];
			message.username=getUsername(from);
			message.roomId=std::to_string((int64_t)innerMessage["chat"]["id"]);
			message.messageId=std::to_string((int64_t)innerMessage["message_id"]);
			message.origin = this;
			message.setDateFromUnixSeconds(innerMessage["date"]);
			
			if(innerMessage.contains("chat")){
				const auto chat=innerMessage["chat"];
				//std::cout<<chat<<std::endl;
				
				if(chat.contains("title")){
					message.roomName=chat["title"];
				}else if(chat.contains("username") && message.roomId!=chat["username"]){
					message.roomName=chat["username"];
				}
			}
			
			if(innerMessage.contains("text")){
				message.body=innerMessage["text"];
				
				//std::cout<<"{"<<message.roomId<<"} "<<message.body<<std::endl;
				processMessage(message);
			}else if(innerMessage.contains("sticker")){
				//TODO
			}else if(innerMessage.contains("new_chat_members")){
				const auto joinEvents=innerMessage["new_chat_members"];
				std::cout<<"Join Event: "<<joinEvents<<std::endl;
				
				for(const auto& joinEvent:joinEvents){
					ChatEvent e;
					e.type=ChatEventType::Join;
					e.username=getUsername(joinEvent);
					e.roomId=message.roomId;
					
					raiseEvent(e);
				}
			}else if(innerMessage.contains("left_chat_member")){
				const auto leaveEvent=innerMessage["left_chat_member"];
				std::cout<<"Leave Event: "<<leaveEvent<<std::endl;
				
				ChatEvent e;
				e.type=ChatEventType::Leave;
				e.username=getUsername(leaveEvent);
				e.roomId=message.roomId;
				
				raiseEvent(e);
			}else{
				std::cout<<"Telegram bot recieved unknown message type: "<<innerMessage<<std::endl;
			}
		}else{
			std::cout<<"Telegram bot recieved unknown update type: "<<messageJSON<<std::endl;
		}
		
		//FIXME: I'm assuming the result array will be in order, i probably shouldnt
		lastMessageId=messageJSON["update_id"];
	}
	
	initialBatchProcessed=true;
	
	return true;
}

CommandAndMessage TelegramBot::splitMessageBodyIfCommand(const std::string& body){
	if(body[0]=='/'){
		auto splitPos=body.find_first_of(' ');
		auto command=body.substr(1,splitPos-1);
		
		const auto usernamePos=lower(command).find('@'+lower(username));
		if(usernamePos!=std::string::npos){
			command.erase(usernamePos);
		}
		
		if(splitPos>=body.size() || splitPos==std::string::npos){
			return std::make_pair(command,"");
		}
		
		return std::make_pair(command,body.substr(splitPos+1));
	}
	return std::nullopt;
}

void TelegramBot::sendMessage(const Message content){
	sendMessage(content.roomId,content.body);
}
void TelegramBot::sendMessage(const std::string chatId,const std::string body){
	auto response=makeApiRequest("sendMessage",{
		{"chat_id",chatId},
		{"text",body}
	});
	
	std::cout<<"Response from Telegram: "<<response<<std::endl;
}

void TelegramBot::sendReply(const Message content,const Message context){
	auto response=makeApiRequest("sendMessage",{
		{"chat_id",context.roomId},
		{"disable_web_page_preview",true},//TODO: this should be configurable somehow
		{"reply_to_message_id",context.messageId},
		{"text",content.body}
	});
	
	std::cout<<"Response from Telegram: "<<response<<std::endl;
}
