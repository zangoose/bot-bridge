/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2020  Jack Phoebus <email>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "message.h"
#include <ctime>
#include "bot.h"
using namespace Bots;

std::string Message::getDateAsString() const{
	const time_t t=std::chrono::system_clock::to_time_t(timestamp);
	const auto utc=gmtime(&t);
	
	std::string timestr;
	timestr.resize(64);
	size_t l;
	
	do{
		timestr.resize(timestr.size()*2);
		l = strftime(&timestr[0], timestr.size(), "%FT%T", utc);
	}while(l==0);
	
	timestr.resize(l);
	
	return timestr;
}

tm Message::getDateObjectUtc() const{
	const time_t tt=std::chrono::system_clock::to_time_t(timestamp);
	return *gmtime(&tt);
}

void Message::setDateFromUnixSeconds(const int64_t t){
	timestamp=std::chrono::system_clock::time_point(std::chrono::seconds(t));
}

void Message::setDateFromUnixMilliseconds(const int64_t t){
	timestamp=std::chrono::system_clock::time_point(std::chrono::milliseconds(t));
}

void Message::sendReply(const Message m) const{
	origin->sendReply(m,*this);
}

void Message::sendReply(const std::string m) const{
	Message response;
	response.roomId=roomId;
	response.body=m;
	
	sendReply(response);
}

