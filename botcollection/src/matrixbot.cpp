/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2020  Jack Phoebus <email>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "matrixbot.h"
#include "curlinstance.h"
#include <iostream>
using namespace Bots;

static bool returnTrueAndSleepIfTimeoutError(const nlohmann::json response){
	if(response.contains("errcode") && response.contains("retry_after_ms")){
		std::cout<<"Matrix timeout error: "<<response<<std::endl;
			
		std::this_thread::sleep_for(std::chrono::milliseconds(response["retry_after_ms"]));
		
		return true;
	}
	return false;
}

static nlohmann::json tryRequest(const std::string& url,const nlohmann::json& args,const std::string& header,const bool forcePost=false){
	CurlInstance curl;
	//TODO: put limit on retry attempts
	while(true){
		try{
			const auto response=curl.makeRequest(url,args,header,forcePost).str();
		
			if(response==""){
				return {};
			}
			
			return nlohmann::json::parse(response);
		}catch(const network_error& err){
			//TODO: other error handling?
			if(err.Code()==429){
				returnTrueAndSleepIfTimeoutError(nlohmann::json::parse(err.Response()));
			}else{
				std::cerr<<err.what();
			}
		}//crash with other errors
	}
}

nlohmann::json MatrixBot::makeApiRequest(const std::string method,nlohmann::json args,const bool forcePost){
	const std::string requestUrl="https://"+domain+"/_matrix/client/r0/"+method;
	
	return tryRequest(requestUrl,args,authHeader,forcePost);
}

nlohmann::json MatrixBot::makeApiQueryRequest(const std::string method,const std::map<std::string,std::string> queryParams,const nlohmann::json args,const bool forcePost){
	std::string query;
	for(const auto& param:queryParams){
		char prepend=query.size()?'&':'?';
		query+=prepend+param.first+"="+CurlInstance::urlEncode(param.second);
	}
	
	const std::string requestUrl="https://"+domain+"/_matrix/client/r0/"+method+query;
	
	return tryRequest(requestUrl,args,authHeader,forcePost);
}

void MatrixBot::processRoomEvents(const std::string& roomState,const nlohmann::json& messages){
	//join
	if(roomState=="join"){
		for(const auto& [room,data]:messages.items()){
			//std::cout<<roomData.key()<<" "<<roomData.value()<<std::endl;
			
			std::string reciptId;
			uint64_t reciptTime=0;
			
			//probably only care about events for the purpose of this bot, but TODO
			//FIXME: this is gross and probably inefficient, better way?
			if(data.contains("timeline") && data["timeline"].contains("events")){
				for(const auto& event:data["timeline"]["events"]){
					//std::cout<<room<<" "<<event<<std::endl;
					
					//TODO: other types of events?
					const std::string type=event["type"];
					//std::cout<<"Got message of type "<<type<<":"<<event<<std::endl;
					
					const uint64_t serverTime=event["origin_server_ts"];
					
					if(type=="m.room.message" && event["content"]["msgtype"]=="m.text"){//TODO: handle other msgtype
						Message message;
						message.body=event["content"]["body"];
						message.username=event["sender"];
						message.roomId=room;
						message.messageId=event["event_id"];
						message.raw=data;
						message.origin=this;
						message.setDateFromUnixMilliseconds(serverTime);
						
						//std::cout<<"got plaintext message: "<<message.body<<std::endl;
						
						if(message.username!=fullUsername && initialBatchProcessed){
							processMessage(message);
						}
					}else if(type=="m.room.encrypted"){
					}
					
					//update receipt
					//TODO: store on disk if a database is set
					//std::cout<<"got timestamp: "<<serverTime<<">"<<reciptTime<<std::endl;
					if(serverTime>reciptTime){
						reciptTime=serverTime;
						reciptId=event["event_id"];
						//std::cout<<"setting recipt to: "<<reciptId<<std::endl;
					}
				}
			}
			
			nlohmann::json test={};
			
			//TODO: send receipt to the room when done
			if(reciptTime){
				const std::string reciptURL="rooms/"+CurlInstance::urlEncode(room)+"/receipt/m.read/"+CurlInstance::urlEncode(reciptId);
				//std::cout<<"sending recipt: "<<reciptURL<<std::endl;
				makeApiRequest(reciptURL,{},true);
			}
		}
	}
	
	//invite
	//leave
}

bool MatrixBot::mainLoop(){
	std::map<std::string,std::string> params{
		{"set_presence","online"}
	};
	
	//TODO: consider ignoring the events from this initial batch
	if(nextBatchId!=""){
		params["since"]=nextBatchId;
	}
	
	auto response=makeApiQueryRequest("sync",params);
	//std::cout<<response<<std::endl;
	
	//next_batch
	nextBatchId=response["next_batch"];
	
	//rooms
	if(response.contains("rooms")){
		const auto rooms=response["rooms"];
		
		for(const auto& roomEvent:rooms.items()){
			processRoomEvents(roomEvent.key(),roomEvent.value());
		}
		
		initialBatchProcessed=true;
	}
	
	//account_data
	//device_lists
	//device_one_time_keys_count
	//groups
	//presence
	//to_device
	
	return true;
}

CommandAndMessage MatrixBot::splitMessageBodyIfCommand(const std::string& body){
	if(body[0]=='!'){
		auto splitPos=body.find_first_of(' ');
		auto command=body.substr(1,splitPos-1);
		
		if(splitPos>=body.size() || splitPos==std::string::npos){
			return std::make_pair(command,"");
		}
		
		return std::make_pair(command,body.substr(splitPos+1));
	}
	return std::nullopt;
}

MatrixBot::MatrixBot(const std::string& label,const std::string& username,const std::string& password,const std::string& botDomain,const std::string& botOwner):MatrixBot(label,username,password,botDomain,"","",botOwner){}

MatrixBot::MatrixBot(const std::string& label, const std::string& username,const std::string& password,const std::string& botDomain,const std::string& deviceName,const std::string& deviceDisplayName,const std::string& botOwner):Bot(label),domain(botDomain),owner(botOwner){
	sleepTimeMilliseconds=10000;
	nlohmann::json args;
	
	std::string displayName=deviceDisplayName;
	if(displayName==""){
		displayName=deviceName;
	}
	
	if(deviceName!=""){
		args["device_id"]=deviceName;
		args["initial_device_display_name"]=displayName;
	}
	
	fullUsername='@'+username+":"+domain;
	
	args["type"]="m.login.password";
	args["identifier"]={{"type","m.id.user"},{"user",username}};
	args["password"]=password;//TODO: clearing this cleanly?
	
	auto response=makeApiRequest("login",args);
	
	//TODO: throw if error logging in
	//TODO: store token on disk and re-use it if possible to avoid the login api
	
	std::cout<<"Matrix bot info: "<<response<<std::endl;
	authHeader="Authorization: Bearer "+response["access_token"].get<std::string>();
}

MatrixBot::~MatrixBot(){
	wipeString(authHeader);
}

void MatrixBot::sendMessage(const Message content){
	sendMessage(content.roomId,content.body);
}

void MatrixBot::sendMessage(const std::string chatId,const std::string body){
	//std::cout<<"Sending: '"<<body<<"' to: "<<chatId<<std::endl;
	const auto method="rooms/"+CurlInstance::urlEncode(chatId)+"/send/m.room.message";
	
	//FIXME: matrix now rate limits message sending, so this should instead be queued and executed by a background thread
	auto response=makeApiRequest(method,{
		{"msgtype","m.text"},
		{"body",body}
	});
	
	std::cout<<"Response from Matrix: "<<response<<std::endl;
}

void MatrixBot::sendReply(const Message content,const Message context){
	//TODO
	std::cout<<"WARNING: Matrix bot does not yet support replies"<<std::endl;
}
