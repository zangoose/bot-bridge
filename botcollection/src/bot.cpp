/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2020  Jack Phoebus <email>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "bot.h"
#include <algorithm>
#include <chrono>
#include <mutex>
#include <iostream>
using namespace Bots;

void Bots::wipeString(std::string& str){
	for(int i=0;i<str.size();i++){
		str[i]=0;
	}
}

std::string Bots::lower(std::string str){
	std::transform(str.begin(),str.end(),str.begin(),[](unsigned char c){return std::tolower(c);});
	return str;
}

static std::filesystem::path dbPath;
std::filesystem::path Bots::getDatabasePath(){
	return dbPath;
}
void Bots::setDatabasePath(const std::string path){
	setDatabasePath(std::filesystem::path(path));
}
void Bots::setDatabasePath(const std::filesystem::path path){
	if(path.empty()){
		dbPath.clear();
		return;
	}
	
	std::cout<<"using DB dir: "<<path<<std::endl;
	//TODO: user-friendly reporting of filesystem related errors
	std::filesystem::create_directories(path);
	dbPath=path;
}

static std::unordered_map<std::string,Bot*> labeledBots;
static std::mutex labelMapLock;

static void printMap(const std::unordered_map<std::string,Bot*>& m){
	for(const auto& x:m){
		std::cout<<"{'"<<x.first<<"','"<<x.second<<"'}"<<std::endl;
	}
}

Bot* Bots::getBotFromLabel(std::string label){
	std::lock_guard lock(labelMapLock);
	//std::cout<<"Trying to fetch "<<label<<" from: "<<std::endl;
	//printMap(labeledBots);
	
	auto value=labeledBots.find(label);
	if(value==labeledBots.end()){
		return nullptr;
	}
	
	return value->second;
}

std::tuple<std::string,std::string> Bots::splitLabelAndRoom(std::string roomTag){
	auto splitPos=roomTag.find_first_of(':');
	auto label=roomTag.substr(0,splitPos);
	auto room=roomTag.substr(splitPos+1);
	
	return std::make_tuple(label,room);
}

Bot::Bot(const std::string& label):botLabel(label){
	if(label.size()==0){
		return;
	}
	
	std::lock_guard lock(labelMapLock);
	
	if(labeledBots.find(label) != labeledBots.end()){
		throw std::runtime_error("Recieved duplicate label: "+label);
	}
	
	labeledBots[label]=this;
	//std::cout<<"added label "<<label<<std::endl;
}

std::string Bot::label(){
	return botLabel;
}

template<typename ... Args>
inline void processCallbacks(std::list<std::function<bool(Args...)>>& callbacks,Args... args){
	//call all callbacks, if the callback returns false, remove it
	callbacks.erase(
		std::remove_if(callbacks.begin(),callbacks.end(),[&](const std::function<bool(Args...)>& callback){
			return !callback(args...);
		}),
		callbacks.end()
	);
}

void Bot::processMessage(Message m){
	const auto command=splitMessageBodyIfCommand(m.body);
	
	//TODO: can this condition be simplified/nested so that command.value() is only called once?  keep in mind the behavior shouldnt change if the command isnt a real command
	if(command.has_value() && commands.find(lower(command.value().first))!=commands.end()){
		std::tie(m.command,m.body)=command.value();
		
		commands[lower(m.command)](m);
	}else{
		processCallbacks(messageCallbacks,m);
	}
}

void Bot::raiseEvent(ChatEvent e){
	processCallbacks(eventCallbacks,e);
}


void Bot::addResponseHandler(Bots::MessageCallback callback){
	messageCallbacks.push_back(callback);
}

void Bots::Bot::addEventHandler(Bots::EventCallback callback){
	eventCallbacks.push_back(callback);
}

void Bots::Bot::addLoopedTask(Bots::LoopedCallback callback){
	loopCallbacks.push_back(callback);
}


void Bot::loopMainUntilFalse(){
	while(mainLoop()){
		//TODO: maybe catch and continue in case of main loop errors
		
		processCallbacks(loopCallbacks);
		
		std::this_thread::sleep_for(std::chrono::milliseconds(sleepTimeMilliseconds));
	}
}

void Bot::setCommandHandler(std::string command, MessageCallback handler){
	commands[lower(command)]=handler;
}

void Bot::start(){
	mainThread=std::thread(&Bot::loopMainUntilFalse,this);
}

void Bot::waitUntilDone(){
	mainThread.join();
}
