/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2020  Jack Phoebus <email>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CURLINSTANCE_H
#define CURLINSTANCE_H

#include <mutex>
#include <string>
#include <sstream>
#include <exception>
#include <nlohmann/json.hpp>

class network_error:public std::runtime_error{
	long code;
	std::string response;
public:
	network_error(int err_code,std::string serverResponse,std::string errorMessage);
	
	long Code() const;
	std::string Response() const;
};

class CurlInstance{
private:
	void* curlHandle;
	std::mutex lock;
	
public:
	CurlInstance();
	~CurlInstance();
	
	static std::string urlEncode(const std::string str);
	
	//TODO:consider splitting into explicit get/post versions
	std::stringstream makeRequest(const std::string& url,const nlohmann::json& args,const std::string& customHeader="",const bool forcePost=false);
};

#endif // CURLINSTANCE_H
