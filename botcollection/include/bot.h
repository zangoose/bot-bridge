/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2020  Jack Phoebus <email>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef BOT_H
#define BOT_H
#include <queue>
#include <string>
#include <functional>
#include <list>
#include <thread>
#include <chrono>
#include <filesystem>
#include <optional>
#include <nlohmann/json.hpp>
#include "message.h"
#include "chatevent.h"

namespace Bots{
	typedef std::function<bool(Message)> MessageCallback;
	typedef std::function<bool(ChatEvent)> EventCallback;
	typedef std::function<bool()> LoopedCallback;
	typedef std::optional<std::pair<std::string,std::string>> CommandAndMessage;
	
	//TODO: Replace with "single instance string" class
	void wipeString(std::string& str);
	
	std::string lower(std::string);
	
	std::filesystem::path getDatabasePath();
	void setDatabasePath(const std::string);
	void setDatabasePath(const std::filesystem::path);
	
	class Bot{
	private:
		std::string botLabel;
		
		std::list<MessageCallback> messageCallbacks;
		std::list<EventCallback> eventCallbacks;
		std::list<LoopedCallback> loopCallbacks;
		std::thread mainThread;
		std::map<std::string,MessageCallback> commands;
		
		void loopMainUntilFalse();
	protected:
		bool initialBatchProcessed=false;
		
		void processMessage(Message);
		void raiseEvent(ChatEvent);
		
		virtual bool mainLoop()=0;
		virtual CommandAndMessage splitMessageBodyIfCommand(const std::string& body)=0;
	public:
		Bot(const std::string& label);
		virtual ~Bot(){}
		
		unsigned long sleepTimeMilliseconds=500;
		
		std::string label();
		
		virtual void sendMessage(const Message content)=0;
		virtual void sendReply(const Message content,const Message context)=0;
		
		void addResponseHandler(MessageCallback);
		void setCommandHandler(std::string command,MessageCallback handler);
		void addEventHandler(EventCallback);
		void addLoopedTask(LoopedCallback);
		
		void start();
		void waitUntilDone();
	};
	
	Bot* getBotFromLabel(std::string label);
	std::tuple<std::string,std::string> splitLabelAndRoom(std::string roomTag);
}
#endif // BOT_H
