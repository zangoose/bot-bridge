/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2020  Jack Phoebus <email>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MATRIXBOT_H
#define MATRIXBOT_H

#include "bot.h"
#include <string>
#include <map>

namespace Bots{
	class MatrixBot: public Bot {
	private:
		//TODO:if possible, nextbatchId should probably be preserved
		std::string authHeader,domain,owner,nextBatchId,fullUsername;
		uint64_t lastMessageId=0;
		
		nlohmann::json makeApiRequest(const std::string method,const nlohmann::json args={},const bool forcePost=false);
		nlohmann::json makeApiQueryRequest(const std::string method,const std::map<std::string,std::string> queryParams,const nlohmann::json args={},const bool forcePost=false);
		
		void processRoomEvents(const std::string& roomState,const nlohmann::json& messages);
	protected:
		bool mainLoop() override;
		CommandAndMessage splitMessageBodyIfCommand(const std::string& body) override;
	public:
		MatrixBot(const std::string& label,const std::string& username,const std::string& password,const std::string& botDomain,const std::string& botOwner="");
		MatrixBot(const std::string& label,const std::string& username,const std::string& password,const std::string& botDomain,const std::string& deviceName,const std::string& deviceDisplayName,const std::string& botOwner="");
		virtual ~MatrixBot() override;
		
		void sendMessage(const Message content) override;
		void sendMessage(const std::string chatId,const std::string body);
		void sendReply(const Message content,const Message context) override;
	};
}

#endif // MATRIXBOT_H
