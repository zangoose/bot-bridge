/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2020  Jack Phoebus <email>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MESSAGE_H
#define MESSAGE_H

#include <string>
#include <chrono>
#include <nlohmann/json.hpp>

namespace Bots{
	class Bot;

	struct Message{
		std::string
			body,
			username,
			roomId,
			roomName,
			messageId,
			command;
		std::chrono::system_clock::time_point timestamp;
		
		nlohmann::json raw;
		Bot* origin=nullptr;
		
		std::string getDateAsString() const;
		tm getDateObjectUtc() const;
		void setDateFromUnixSeconds(const int64_t);
		void setDateFromUnixMilliseconds(const int64_t);
		
		void sendReply(const Message m) const;
		void sendReply(const std::string m) const;
	};
}

#endif // MESSAGE_H
