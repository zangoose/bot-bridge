/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2020  Jack Phoebus <email>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TELEGRAMBOT_H
#define TELEGRAMBOT_H

#include "bot.h"
#include <string>

namespace Bots{
	class TelegramBot: public Bot {
	private:
		std::string token,owner,username;
		uint64_t lastMessageId=0;
		
		nlohmann::json makeApiRequest(const std::string method,nlohmann::json args={});
	protected:
		bool mainLoop() override;
		
		CommandAndMessage splitMessageBodyIfCommand(const std::string& body) override;
	public:
		TelegramBot(const std::string& label,const std::string& botToken,const std::string& botOwner="");
		virtual ~TelegramBot() override;
		
		void sendMessage(const Message content) override;
		void sendMessage(const std::string chatId,const std::string body);
		void sendReply(const Message content,const Message context) override;
	};
}

#endif // TELEGRAMBOT_H
